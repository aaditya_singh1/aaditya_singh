const express = require("express");
const { MongoClient } = require("mongodb");
const { collection } = require("./models/companyschema");

const app = express();


const port = 4000;
const mongoUrl = "mongodb://127.0.0.1:27017";
const dbName = "ads";

app.use(express.json());


app.get("/search", async (req, res) => {
  const client = new MongoClient(mongoUrl);

  try {
    await client.connect();

    const db = client.db(dbName);
    const collection = db.collection("ads");

    const query = req.query.q;
    const cursor = collection.aggregate([
        {
          $lookup: {
            from: "companies",
            localField: "companyId",
            foreignField: "_id",
            as: "company",
          },
        },
        { $unwind: "$company" },
        {
          $match: {
            $or: [
              { "company.name": { $regex: query, $options: "i" } },
              { primaryText: { $regex: query, $options: "i" } },
              { headline: { $regex: query, $options: "i" } },
              { description: { $regex: query, $options: "i" } },
            ],
          },
          
        },
        {
            $project: {
                _id: 1,
                headline:1,
                description:1,
                primaryText:1,
                name: 1,
                imageUrl: 1
            }
        },
      ]);


    const results = await cursor.toArray();

    res.json(results);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal server error");
  } finally {
    console.log(collection)
    await client.close();
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});







// app.use((request,response, next)=>{
//     response.setHeader("Access-Control-Allow-Origin", "*")
//     response.setHeader("Access-Control-Allow-Headers", "*")
//     response.setHeader("Access-Control-Allow-Methods", "*")
//     next();
// })