const mongoose = require('mongoose');

const adschema = new mongoose.Schema({
  headline: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  primaryText: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  companyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company'
  }
});

const Ad = mongoose.model('ads', adschema);

module.exports = Ad;